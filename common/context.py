from behave.runner import Context
from playwright.async_api import Browser, BrowserContext, Page

class EnhancedContext(Context):
  browser: Browser
  browser_context: BrowserContext
  page: Page
  target_browser: str
