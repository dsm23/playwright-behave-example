from typing import AsyncGenerator
from behave import fixture
from playwright.async_api import async_playwright, Browser

from common.context import EnhancedContext

def config():
    return {
        "headless": True,
        "slow_mo": 100
    }

@fixture(name="fixture.browser.firefox")
async def browser_firefox(ctx: EnhancedContext) -> AsyncGenerator[Browser, None]:
    async with async_playwright() as playwright:
        ctx.browser = await playwright.firefox.launch(
            **config()
        )

        ctx.browser_context = await ctx.browser.new_context(
            # storage_state="login.json"
        )

        yield ctx.browser

        await ctx.browser.close()

@fixture(name="fixture.browser.chromium")
async def browser_chromium(ctx: EnhancedContext) -> AsyncGenerator[Browser, None]:
    async with async_playwright() as playwright:
        ctx.browser = await playwright.chromium.launch(
            **config()
        )

        ctx.browser_context = await ctx.browser.new_context(
            # storage_state="login.json"
        )

        yield ctx.browser

        await ctx.browser.close()

@fixture(name="fixture.browser.webkit")
async def browser_webkit(ctx: EnhancedContext) -> AsyncGenerator[Browser, None]:
    async with async_playwright() as playwright:
        ctx.browser = await playwright.webkit.launch(
            **config()
        )

        ctx.browser_context = await ctx.browser.new_context(
            # storage_state="login.json"
        )

        yield ctx.browser

        await ctx.browser.close()

@fixture(name="fixture.browser.chrome")
async def browser_chrome(ctx: EnhancedContext) -> AsyncGenerator[Browser, None]:
    async with async_playwright() as playwright:
        ctx.browser = await playwright.chromium.launch(
            **config(),
            channel="chrome"
        )

        ctx.browser_context = await ctx.browser.new_context(
        #     storage_state="login.json"
        )

        yield ctx.browser

        await ctx.browser.close()

@fixture(name="fixture.browser.edge")
async def browser_edge(ctx: EnhancedContext) -> AsyncGenerator[Browser, None]:
    async with async_playwright() as playwright:
        ctx.browser = await playwright.chromium.launch(
            **config(),
            channel="msedge"
        )

        ctx.browser_context = await ctx.browser.new_context(
            # storage_state="login.json"
        )

        yield ctx.browser

        await ctx.browser.close()
