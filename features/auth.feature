Feature: Login
  Scenario: Get Key
    Given I am on the webauthn page
    When I type python-example into the input field #input-email
    And I select direct into the select field #select-attestation
    And I click the register button
    And I click the login button
    Then I am redirected

  Scenario: Have Key
    Given I am on the webauthn page
    When I type python-example into the input field #input-email
    And I select direct into the select field #select-attestation
    And I click the login button
    Then I am redirected
