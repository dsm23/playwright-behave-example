import asyncio
import inspect
from typing import AsyncGenerator, Callable, Dict
from behave.runner import Context
from playwright.async_api import Browser

from common.context import EnhancedContext
from common.fixtures import (
    browser_chrome,
    browser_chromium,
    browser_edge,
    browser_firefox,
    browser_webkit
)
fixture_registry : Dict[str, Callable[[Context], AsyncGenerator[Browser, None]]] = {
    "chrome": browser_chrome,
    "chromium": browser_chromium,
    "edge": browser_edge,
    "firefox": browser_firefox,
    "webkit": browser_webkit,
}

def is_gen_func(func: AsyncGenerator[Browser, None]) -> bool:
    return inspect.isasyncgenfunction(func)

async def setup_fixture(
    fixture_func: Callable[[Context], AsyncGenerator[Browser, None]],
    ctx: Context
) -> AsyncGenerator[Browser, None]:
    if is_gen_func(fixture_func):
        async def cleanup_fixture():
            try:
                await func_it.__anext__()
            except StopAsyncIteration:
                return False
        func_it: AsyncGenerator = fixture_func(ctx)
        ctx.add_cleanup(cleanup_fixture)
        setup_result = await func_it.__anext__()
    else:
        setup_result = fixture_func(ctx)

    return setup_result

def before_all(ctx: EnhancedContext) -> None:
    ctx.target_browser = ctx.config.userdata.get("browser")

def before_feature(ctx: Context, _) -> None:
    fixture_func = fixture_registry.get(ctx.target_browser, None)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(setup_fixture(fixture_func, ctx))
