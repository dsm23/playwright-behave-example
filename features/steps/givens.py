from behave import given
from behave.api.async_step import async_run_until_complete

from common.context import EnhancedContext

@given("I am on the webauthn page")
@async_run_until_complete
async def step_impl(ctx: EnhancedContext):
    ctx.page = await ctx.browser_context.new_page()
    await ctx.page.goto("https://webauthn.io")

    client = await ctx.page.context().new_cdp_session(ctx.page)

    await client.send("WebAuthn.enable")

    auth = await client.send(
        "WebAuthn.addVirtualAuthenticator",
        {
            "options": {
                "protocol": "ctap2",
                "transport": "usb",
            },
        }
    )
