from behave import then
from behave.api.async_step import async_run_until_complete

from common.context import EnhancedContext

@then("I am redirected")
@async_run_until_complete
async def step_impl(ctx: EnhancedContext):
    await ctx.page.is_visible(".party-cat")

