from behave import when
from behave.api.async_step import async_run_until_complete

from common.context import EnhancedContext

@when("I type {input_text} into the input field {input_field}")
@async_run_until_complete
async def step_impl(ctx: EnhancedContext, input_text: str, input_field: str):
    await ctx.page.type(input_field, input_text)

@when("I select {select_value} into the select field {select_field}")
@async_run_until_complete
async def step_impl(ctx: EnhancedContext, select_value: str, select_field: str):
    await ctx.page.select_Option(select_field, select_value)

@when("I click the register button")
@async_run_until_complete
async def step_impl(ctx: EnhancedContext):
    await ctx.page.click("#register-button")
    assert await ctx.page.is_visible(".popover-body")

@when("I click the login button")
@async_run_until_complete
async def step_impl(ctx: EnhancedContext):
    async with ctx.page.expect_navigation():
        await ctx.page.click("#login-button")

